
import {slugger} from './index.js'
describe('test slugget', () => {
  
    /**
     * @it - unit tests can use the 'it' syntax
     */
    it('slugger can slug string with spaces', () => {
       
        expect(slugger("1 2 3")).toEqual("1-2-3");
    })
    /**
     * @test - unit test can use the 'test' syntax
     */
    test('slugger can slug any number of spacy strings', () => {
        
        expect(slugger("0","1 2 3","4")).toEqual("0-1-2-3-4");
    })
})
