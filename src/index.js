export default function slugger(...strParams){
    return strParams.reduce((acc, item) =>{
        acc.push(item.split(" ").join("-"))
        return acc;
    },[]).join("-")
}
